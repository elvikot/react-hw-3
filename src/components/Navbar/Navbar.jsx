import { NavLink } from "react-router-dom";
import "./Navbar.scss";
import FavorIcon from "../FavorIcon/FavorIcon";
import CartIcon from "../CartIcon/CartIcon";

export default function Navbar(props) {
  const { cartAmount, favorAmount } = props;

  return (
    <nav className="navbar">
      <NavLink className="navbar__link" to="/">
        Home
      </NavLink>
      <NavLink className="navbar__link" to="/cart">
        Cart
      </NavLink>
      <NavLink className="navbar__link" to="/favor">
        Favor
      </NavLink>
      <div className="inner-wrapper">
        <FavorIcon productsInFavorAmount={Number(favorAmount)} />
        <CartIcon productInCartAmount={Number(cartAmount)} />
      </div>
    </nav>
  );
}
