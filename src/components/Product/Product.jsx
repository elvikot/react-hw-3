import "./Product.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";

export default function Product(props) {
  const {
    title,
    url,
    vendorCode,
    price,
    color,
    inCart = false,
    inFavor = false,
    cardStatus = true,
  } = props;

  const data = {
    title,
    url,
    vendorCode,
    price,
    color,
  };

  const addToCart = () => {
    props.openModal(data, "addingProduct");
  };

  const removeFromCart = () => {
    props.openModal(data, "deletingProduct");
  };

  const addToFavor = () => {
    props.addToFavor(data);
  };

  const removeFromFavor = () => {
    props.removeFromFavor(data);
  };

  return (
    <div className="product-item">
      {cardStatus ? (
        inFavor ? (
          <span className="star active" onClick={removeFromFavor}></span>
        ) : cardStatus ? (
          <span className="star" onClick={addToFavor}></span>
        ) : null
      ) : inFavor ? (
        <span className="star active disaction"></span>
      ) : cardStatus ? (
        <span className="star disaction"></span>
      ) : null}
      <div className="product-item__img-wrapper">
        <img alt="" className="product-item__img" src={url} />
      </div>
      <div className="product-item__info">
        <p className="product-item__title">{title}</p>
        <p className="product-item__vendor-code">Vendor code: {vendorCode}</p>
        <p className="product-item__color">Color: {color}</p>
        <p className="product-item__price">₴{price}</p>
        {cardStatus ? (
          inCart ? (
            <Button
              className="product-item__cart added"
              text="x"
              onClick={removeFromCart}
            />
          ) : (
            <Button
              className="product-item__cart"
              text="Add to cart"
              onClick={addToCart}
            />
          )
        ) : null}
      </div>
    </div>
  );
}

Product.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  vendorCode: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
};
