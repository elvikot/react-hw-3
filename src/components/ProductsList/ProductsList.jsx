import "./ProductsList.scss";
import { useEffect, useState } from "react";
import Product from "../Product/Product";
import Modal from "../Modal/Modal";

export default function ProductsList({ productsArr, actions }) {
  const [openedModalStatus, setOpenedModalStatus] = useState(false);
  const [userAction, setUserAction] = useState("");
  const [modalContent, setModalContent] = useState({});
  const [selectedProduct, setSelectedProduct] = useState("");

  const { setCartAmount, setFavorAmount } = actions;

  const closeModal = (e) => {
    e.preventDefault();
    if (e.target === e.currentTarget) {
      setOpenedModalStatus(false);
      setModalContent({});
      document.body.style.overflow = "unset";
    }
  };

  const openModal = (product, action) => {
    setOpenedModalStatus(true);
    setUserAction(action);
    setSelectedProduct(product);
    action === "addingProduct"
      ? setModalContent({
          title: "Confirm adding",
          description: "Do you want to add a product to your cart?",
        })
      : setModalContent({
          title: "Confirm deleting",
          description: "Do you want to delete a product from your cart?",
        });
    document.body.style.overflow = "hidden";
  };

  const addToCart = () => {
    const data = JSON.parse(localStorage.getItem("productsInCart"));
    data.push(selectedProduct);
    localStorage.setItem("productsInCart", JSON.stringify(data));
    setCartAmount(data.length);
    setOpenedModalStatus(false);
  };

  const removeFromCart = () => {
    const data = JSON.parse(localStorage.getItem("productsInCart"));

    const productToRemoveIdx = data.findIndex(
      (obj) => obj.vendorCode === selectedProduct.vendorCode
    );
    if (productToRemoveIdx !== -1) {
      data.splice(productToRemoveIdx, 1);
    }
    localStorage.setItem("productsInCart", JSON.stringify(data));
    setCartAmount(data.length);
    setOpenedModalStatus(false);
  };

  const addToFavor = (product) => {
    const data = JSON.parse(localStorage.getItem("productsInFavor"));
    data.push(product);
    localStorage.setItem("productsInFavor", JSON.stringify(data));
    setFavorAmount(data.length);
  };

  const removeFromFavor = (product) => {
    const data = JSON.parse(localStorage.getItem("productsInFavor"));
    const productToRemoveIdx = data.findIndex(
      (obj) => obj.vendorCode === product.vendorCode
    );

    if (productToRemoveIdx !== -1) {
      data.splice(productToRemoveIdx, 1);
    }

    localStorage.setItem("productsInFavor", JSON.stringify(data));
    setFavorAmount(data.length);
  };

  const cartData = JSON.parse(localStorage.getItem("productsInCart"));
  const favorData = JSON.parse(localStorage.getItem("productsInFavor"));

  return (
    <>
      <div className="products-list">
        {productsArr.map((product, id) => (
          <Product
            inCart={
              cartData.findIndex(
                (obj) => obj.vendorCode === product.vendorCode
              ) !== -1
            }
            inFavor={
              favorData.findIndex(
                (obj) => obj.vendorCode === product.vendorCode
              ) !== -1
            }
            removeFromCart={removeFromCart}
            addToFavor={addToFavor}
            openModal={openModal}
            removeFromFavor={removeFromFavor}
            key={id}
            title={product.title}
            url={product.url}
            vendorCode={product.vendorCode}
            color={product.color}
            price={Number(product.price)}
          />
        ))}
      </div>
      {openedModalStatus ? (
        <Modal
          title={modalContent.title}
          content={modalContent.description}
          action={userAction === "addingProduct" ? addToCart : removeFromCart}
          closeModal={closeModal}
          btns={["Confirm", "Cancel"]}
        />
      ) : null}
    </>
  );
}
