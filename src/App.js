import "./App.scss";
import { NavLink, Routes, Route } from "react-router-dom";
import { CartPage, HomePage, ErrorPage, FavorPage } from "./pages";
import Navbar from "./components/Navbar/Navbar";
import { useEffect, useState } from "react";

export default function App(props) {
  const [productsInCart, setProductsInCart] = useState(0);
  const [productsInFavor, setProductsInFavor] = useState(0);
  const cart = JSON.parse(localStorage.getItem("productsInCart"));
  const favor = JSON.parse(localStorage.getItem("productsInFavor"));

  function setProductsInCartAmount(amount) {
    setProductsInCart(amount);
  }
  function setProductsInFavorAmount(amount) {
    setProductsInFavor(amount);
  }

  useEffect(() => {
    setProductsInCart(cart.length);
  }, [cart]);
  useEffect(() => {
    setProductsInFavor(favor.length);
  }, [favor]);

  const propsToSend = {
    setCartAmount: setProductsInCartAmount,
    setFavorAmount: setProductsInFavorAmount,
  };

  return (
    <>
      <Navbar cartAmount={productsInCart} favorAmount={productsInFavor} />
      <Routes>
        <Route path="/" element={<HomePage actions={propsToSend} />} />
        <Route path="/cart" element={<CartPage actions={propsToSend} />} />
        <Route path="/favor" element={<FavorPage actions={propsToSend} />} />
        <Route path="*" element={<ErrorPage />} />
      </Routes>
    </>
  );
}
