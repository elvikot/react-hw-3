import ProductsList from "../components/ProductsList/ProductsList";
import { useEffect, useState } from "react";

export function HomePage({ actions }) {
  const { setCartAmount, setFavorAmount } = actions;
  const [productsArr, setProductsArr] = useState([]);

  useEffect(() => {
    fetch("./products.json")
      .then((res) => res.json())
      .then((data) => setProductsArr(data));

    const cart = JSON.parse(localStorage.getItem("productsInCart"));

    if (!cart) {
      localStorage.setItem("productsInCart", JSON.stringify([]));
      setCartAmount(0);
    } else {
      setCartAmount(cart.length);
    }

    const favor = JSON.parse(localStorage.getItem("productsInFavor"));
    if (!favor) {
      localStorage.setItem("productsInFavor", JSON.stringify([]));
      setFavorAmount(0);
    } else {
      setFavorAmount(favor.length);
    }
  }, []);

  return <ProductsList productsArr={productsArr} actions={actions} />;
}
